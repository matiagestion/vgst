<!DOCTYPE html>
<html lang="en">
<head>
  <title>Consultar tablas | vGst</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,300,0,0" />
	<link rel="stylesheet" href="https://bootswatch.com/5/zephyr/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php
// Te recomiendo utilizar esta conección, la que utilizas ya no es la recomendada. 
$link = new PDO('mysql:host=mysql.ejemplo.es;dbname=infoempresa', 'usuario1234', 'contraseña123');
?>
<link rel="stylesheet" href="https://bootswatch.com/5/zephyr/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">MatiaGestión</a> 
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor02">
      <ul class="navbar-nav me-auto">
        <li class="nav-item">
          <a class="nav-link active" href="index.php">Inicio
            <span class="visually-hidden">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="edificios.php">Edificios</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="consultartablas.php">Consultar tablas</a>
        </li>
        <li class="nav-item">
        </li>
 <li class="nav-item">
          <a class="nav-link" href="#"></a>
        </li>
 <li class="nav-item">
          <a class="nav-link" href="#">      </a>
        </li>
 <li class="nav-item">
          <a class="nav-link" href="#">      </a>
        </li>
 <li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
        </li><li class="nav-item">
          
    
          
          
        </li>
<button type="button" class="btn btn-light" href="ajustes.php"><span class="material-symbols-outlined">
settings</button>
 </li><li class="nav-item"> </li><li class="nav-item">
<button type="button" class="btn btn-light" href="administrador.php"><span class="material-symbols-outlined">
admin_panel_settings
</span></button>
</span>
 </li><li class="nav-item"> </li><li class="nav-item">
<button type="button" class="btn btn-light" href="usuario.php"><span class="material-symbols-outlined">
account_circle
</span></button>
    </div>
  </div>
</div>
</nav>
<table class="table table-striped">
  	
		<thead>
		<tr>
			<th><span class="material-symbols-outlined">
location_on
</span>DIRECCIÓN</th>
			<th><span class="material-symbols-outlined">
badge
</span>NOMBRE</th>
			<th><span class="material-symbols-outlined">
fingerprint
</span>CIF</th>
			
		</tr>
		</thead>
<?php foreach ($link->query('SELECT * from edificios') as $row){ // aca puedes hacer la consulta e iterarla con each. ?> 
<tr>
	<td><?php echo $row['direccion'] // aca te faltaba poner los echo para que se muestre el valor de la variable.  ?></td>
    <td><?php echo $row['nombre'] ?></td>
    <td><?php echo $row['cif'] ?></td>
 </tr>
<?php
	}
?>
</table>
</body>
</html>
